SOURCE_FILES += main.cpp
LIBRARIES += -lbluetooth
WARNINGS = -Wall
DEBUG = -ggdb
CC = g++
all: main.cpp
	$(CC) $(WARNINGS) $(DEBUG) $(SOURCE_FILES) $(LIBRARIES) -o bluetooth
clean:
	rm ./bluetooth
