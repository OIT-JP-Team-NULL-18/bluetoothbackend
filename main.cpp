#include <stdio.h>
#include <fstream>
#include <unistd.h>
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>
using std::ofstream;
void check(void);
void getnext(void);
char nextcommand[50000];
int bytesunprocessed;
char buf[50000];
int main(int argc, char **argv)
{
    ofstream route;
    ofstream stop;
    bytesunprocessed = 0;
    char address[18] = "B8:27:EB:5F:9B:62";
    struct sockaddr_rc loc_addr = { 0 }, rem_addr = { 0 };
    int s, client, bytes_read;
    socklen_t opt = sizeof(rem_addr);

    // allocate socket
    s = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);

    // bind socket to port 1 of the first available 
    // local bluetooth adapter
    loc_addr.rc_family = AF_BLUETOOTH;
    str2ba(address, &loc_addr.rc_bdaddr);
    //loc_addr.rc_bdaddr = *BDADDR_ANY;
    loc_addr.rc_channel = (uint8_t) 1;
    bind(s, (struct sockaddr *)&loc_addr, sizeof(loc_addr));

    // put socket into listening mode
    listen(s, 1);

    // accept one connection
    client = accept(s, (struct sockaddr *)&rem_addr, &opt);

    ba2str( &rem_addr.rc_bdaddr, buf );
    fprintf(stderr, "accepted connection from %s\n", buf);
    memset(buf, 0, sizeof(buf));
    memset(nextcommand,0,sizeof(nextcommand));
    bool flag = false;
    // read data from the client
    while(flag == false)
    {
      bytes_read = read(client, &buf[bytesunprocessed], sizeof(buf)-bytesunprocessed);
      if( bytes_read > 0 )
	{
	  bytesunprocessed = bytesunprocessed + bytes_read;
	  printf("buffer: %s\n", buf);
	}
      else
	{
	  flag = true;
	}
      //check();
      if(flag == false)
	{
	  getnext();
	  //check();
	  if(strcmp(nextcommand,"RouteFile") == 0)
	    {
	      route.open("routes.txt" , std::ios::out | std::ios::trunc);
	      //route file detected read and save route file
	      if(route.is_open())
	      {
		while(flag == false)
		{
		  //check();
		  if(bytesunprocessed ==0)
		    {
		      bytes_read = read(client, &buf[bytesunprocessed], sizeof(buf)-bytesunprocessed);
		      
		      if( bytes_read > 0 )
			{
			  
			  bytesunprocessed = bytesunprocessed + bytes_read;
			}
		      
		    }
		  //check();
		  getnext();
		  //check();
		  if(strcmp(nextcommand,"EOF") == 0)
		    {
		      //end of file detected dave route file
		      flag = true;
		      memset(nextcommand,0,sizeof(nextcommand));
		    }
		  else
		    {
		      //additional route detected append to route file
		      //printf("nextcommand: %s", nextcommand);
		      //fflush(stdout);
		      route <<nextcommand;
		      route << std::endl;
		      memset(nextcommand,0,sizeof(nextcommand));
		    }
		}
		route.close();
	      }
	      flag = false;  
	    }
	  else if(strcmp(nextcommand,"StopFile")== 0)
	    {
	      stop.open("stops.txt" , std::ios::out | std::ios::trunc);
	      //stop file detected read and save stop file
	      if(stop.is_open())
	      {
		while(flag == false)
		{
		  //check();
		  if(bytesunprocessed ==0)
		    {
		      bytes_read = read(client, &buf[bytesunprocessed], sizeof(buf)-bytesunprocessed);
		      
		      if( bytes_read > 0 )
			{
			  
			  bytesunprocessed = bytesunprocessed + bytes_read;
			}
		      
		    }
		  //check();
		  getnext();
		  //check();
		  if(strcmp(nextcommand,"EOF") == 0)
		    {
		      //end of file detected dave route file
		      flag = true;
		      memset(nextcommand,0,sizeof(nextcommand));
		    }
		  else
		    {
		      //additional stop detected append to stop file
		      //printf("nextcommand: %s", nextcommand);
		      //fflush(stdout);
		      stop << nextcommand;
		      stop << std::endl;
		      memset(nextcommand,0,sizeof(nextcommand));
		    }
		  
		}
		stop.close();
	      }
	      flag = false;  
	    }
	  
	}
    }
    // close connection
    close(client);
    close(s);
    return 0;
}

void getnext(void)
{
  int numread = 0;
  if(bytesunprocessed <= 0)
    {
      printf("ERROR");
      fflush(stdout);
    }
  while(buf[numread] != '\n' && numread < 49997)
    {
      nextcommand[numread] = buf[numread];
      numread += 1;
    }
  nextcommand[numread + 1] = '\0';
  memmove(buf,buf+numread+1,sizeof(buf) - numread - 1);
  bytesunprocessed = bytesunprocessed - numread - 1;
}
void check()
{
  if(bytesunprocessed > 49999) printf("ERROR bytesunprocessed");
  printf("buffer: %s",buf);
  fflush(stdout);
  printf("nextcommand: %s",nextcommand);
  fflush(stdout);

}
